using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Enumeration for player states
    public enum PlayerState
    {
        Idle,
        Walking,
        Running,
        Jumping
    }

    // Public variables
    public float moveSpeed = 5f;
    public float jumpForce = 10f;

    // Private variables
    private Rigidbody rb;
    private PlayerState currentState = PlayerState.Idle;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        // Move player based on current state
        MovePlayer();

        // Check for input to change player state
        UpdatePlayerState();
        Debug.Log(currentState);
    }

    void MovePlayer()
    {
        float moveInputX = Input.GetAxisRaw("Horizontal");
        float moveInputZ = Input.GetAxisRaw("Vertical");

        Vector3 movement = new Vector3(moveInputX, 0.0f, moveInputZ) * moveSpeed * Time.deltaTime;
        transform.Translate(movement);
    }

    void UpdatePlayerState()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
        }

        if (Mathf.Abs(Input.GetAxisRaw("Horizontal")) > 0f || Mathf.Abs(Input.GetAxisRaw("Vertical")) > 0f)
        {
            if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
            {
                currentState = PlayerState.Running;
            }
            else
            {
                currentState = PlayerState.Walking;
            }
        }
        else
        {
            currentState = PlayerState.Idle;
        }
    }

    void Jump()
    {
        if (currentState != PlayerState.Jumping)
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            currentState = PlayerState.Jumping;
        }
        Debug.Log(currentState);
    }
}